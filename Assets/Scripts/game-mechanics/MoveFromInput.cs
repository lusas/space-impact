﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO limit movement to screen.

public class MoveFromInput : MonoBehaviour
{
	[SerializeField] float speedMultiplier = 5f;

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		transform.Translate(moveHorizontal * speedMultiplier, moveVertical * speedMultiplier, 0);
	}
}
