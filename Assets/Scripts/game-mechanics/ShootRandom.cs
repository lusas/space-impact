﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootRandom : MonoBehaviour
{
	public Transform bulletParent;
	[SerializeField] GameObject bulletPrefab;

	[SerializeField] float minDelay;
	[SerializeField] float maxDelay;

	[SerializeField] Vector2 speed;

	IEnumerator Cooldown()
	{
		float normalized = Random.value;
		float delayTime = Mathf.Lerp(minDelay, maxDelay, normalized);

		yield return new WaitForSeconds(delayTime);
		Shot();
	}

	void Shot()
	{
		GameObject bullet;
		bullet = Instantiate(bulletPrefab, transform.position, transform.localRotation);
		bullet.transform.SetParent(bulletParent);
		bullet.GetComponent<Bullet>().Speed = speed;

		StartCoroutine("Cooldown");
	}

	void Start()
	{
		// starts infinate loop till death.
		StartCoroutine("Cooldown");
	}
}
