﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveConstant : MonoBehaviour
{
	[SerializeField] Vector2 speed;

	void FixedUpdate()
	{
		transform.Translate(speed.x, speed.y, 0);
	}
}
