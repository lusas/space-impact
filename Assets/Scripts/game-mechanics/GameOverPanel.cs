﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{

	[SerializeField] Text textScore;

	public void Display(int score)
	{
		this.gameObject.SetActive(true);
		textScore.text = "Score: " + score.ToString();
	}

}
