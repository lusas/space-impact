﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface I_Damagable
{
	void Damage(int dmgPoints);
}

public enum ObjectType
{
	Friendly,
	Enemy
}

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
	[SerializeField] protected int damage;
	[SerializeField] protected ObjectType type;

	public Vector2 Speed
	{
		get{ return speed; }
		set{ speed = value; }
	}

	Vector2 speed;
	RectTransform rt;

	void Start()
	{
		rt = GetComponent<RectTransform>();
		// Workaround for mono 
		InternalStart();
	}

	void Update()
	{
		rt.Translate(speed.x, speed.y, 0);
		// Workaround for mono 
		InternalUpdate();
	}

	virtual protected void InternalStart()
	{

	}

	virtual protected void InternalUpdate()
	{
	}
}