﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, I_Damagable
{
	[SerializeField]int reward = 100;

	public void Damage(int dmgPoints)
	{
		GameManager.instance.UpdateScore(reward);
		Destroy(this.gameObject);
	}
}
