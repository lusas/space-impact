﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;

	[SerializeField] Text textHealth;
	[SerializeField] Text textScore;

	[SerializeField] GameOverPanel panel_gameover;
	[SerializeField] EnemySpawner spawner;

	int score = 0;
	int health = 5;

	void Start()
	{
		instance = this;
		spawner.StartSpawning();
		// initialize
		UpdateScore(0);
		UpdateHealth(0);
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	public void UpdateScore(int scoreValue)
	{
		score += scoreValue;
		textScore.text = score.ToString();
	}

	public void UpdateHealth(int damage)
	{
		if (health - damage <= 0) GameOver();
		else
		{
			health -= damage;
			textHealth.text = health.ToString();
		}
	}

	public void GameOver()
	{
		textHealth.text = "0";
		spawner.StopSpawning();
		spawner.KillAllSpawns();
		panel_gameover.Display(score);
		DataManager.instance.SaveHighscore(score);
		//display end.
		//destroy shit

	}
}
