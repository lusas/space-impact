﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFromInput : MonoBehaviour
{
	[Tooltip("Cooldown between shots in seconds.")]
	[SerializeField] float shootingCooldown = 1f;

	[SerializeField] GameObject bulletPrefab;
	[SerializeField] Transform bulletParent;

	[SerializeField] Vector2 speed;

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.Space)) Shot();
	}

	bool cooldown = false;

	void Shot()
	{
		if (!cooldown)
		{
			cooldown = true;
			// initialize bullet
			GameObject bullet;
			bullet = Instantiate(bulletPrefab, transform.position, transform.localRotation);
			bullet.transform.SetParent(bulletParent);
			bullet.GetComponent<Bullet>().Speed = speed;

			// start coolind down
			StartCoroutine("CoolingDown");
		}
	}

	IEnumerator CoolingDown()
	{
		yield return new WaitForSeconds(shootingCooldown);
		cooldown = false;
	}
}
