﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSimple : Bullet
{
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Limits") Destroy(this.gameObject);
		else if (type == ObjectType.Friendly && other.gameObject.tag == "Enemy")
		{
			other.GetComponent<I_Damagable>().Damage(damage);
			Destroy(this.gameObject);
		}
		else if (type == ObjectType.Enemy && other.gameObject.tag == "Player")
		{
			other.GetComponent<I_Damagable>().Damage(damage);
			Destroy(this.gameObject);
		}
	}
}
