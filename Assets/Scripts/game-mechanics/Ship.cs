﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship : MonoBehaviour, I_Damagable
{
	public void Damage(int dmgPoints)
	{
		GameManager.instance.UpdateHealth(dmgPoints);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Enemy") GameManager.instance.GameOver();
	}
}
