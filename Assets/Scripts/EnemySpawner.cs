﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	[Range(0.1f, 0.5f)]
	[SerializeField] float minDelay;
	[Range(0.5f, 1f)]
	[SerializeField] float maxDelay;

	[SerializeField] GameObject enemyPrefab;

	Vector3 spawnPosition;
	[SerializeField]RectTransform rect;

	public void StartSpawning()
	{
		// starts infinate loop till death.
		StartCoroutine("Cooldown");
	}

	public void StopSpawning()
	{
		StopAllCoroutines();
	}

	public void KillAllSpawns()
	{
		foreach (Transform item in this.transform.GetComponentsInChildren<Transform>())
		{
			if (item.gameObject != this.gameObject) Destroy(item.gameObject);
		}
	}

	void Spawn()
	{

		GameObject enemy;

		float halfHeight = rect.sizeDelta.y;
//		print(transform.position);

		spawnPosition = new Vector3(
			transform.position.x,
			transform.position.y - Random.Range(-halfHeight, halfHeight),
			transform.position.z
		);
			
		enemy = Instantiate(enemyPrefab, spawnPosition, transform.localRotation);
		enemy.transform.SetParent(this.transform);
		// TODO solve. dirty.
		enemy.GetComponent<ShootRandom>().bulletParent = this.transform;

		StartCoroutine("Cooldown");
	}

	IEnumerator Cooldown()
	{
		float normalized = Random.value;
		float delayTime = Mathf.Lerp(minDelay, maxDelay, normalized);

		yield return new WaitForSeconds(delayTime);
		Spawn();
	}
}
