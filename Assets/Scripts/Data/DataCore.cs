﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to put all base inharitance classes and classes that do not inherit from monobehaviour.

[System.Serializable]
public class Data_Hiscore
{
	public List<int> data;
}
