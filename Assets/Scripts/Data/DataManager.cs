﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
	public static DataManager instance;

	string pp_key = "Hiscore";
	string jsonString;

	// To minimize calls to PP.
	bool pp_loaded = false;

	Data_Hiscore hiscores;

	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
	}

	void Start()
	{
		instance = this;
	}

	public Data_Hiscore GetHighscore()
	{
		// loads from PP once and then uses hiscores object. No calls to PP.
		if (pp_loaded) return hiscores;
		else
		{
			jsonString = PlayerPrefs.GetString(pp_key, "");

			if (jsonString == "")
			{
				Debug.LogWarning("No record of hiscore found! Creating new!");
				hiscores = new Data_Hiscore();
				hiscores.data = new List<int>();
			}
			else
			{
				hiscores = JsonUtility.FromJson<Data_Hiscore>(jsonString);
				pp_loaded = true;
					
			}
			return hiscores;	
		}
	}

	public void SaveHighscore(int score)
	{
		hiscores = GetHighscore();
			
		hiscores.data.Add(score);

		jsonString = JsonUtility.ToJson(hiscores);
		PlayerPrefs.SetString(pp_key, jsonString);
		Debug.Log("Hi-score saved: " + score);
	}
}
