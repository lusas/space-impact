﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiscoreListDisplay : MonoBehaviour
{
	[SerializeField] Transform content;
	[SerializeField] GameObject prefab;

	void OnEnable()
	{
		Initialize();
	}

	void OnDisable()
	{
		DestroyAll();
	}

	// TODO make it smarter, so that it doesnt delete all records and makes new ones, but replaces + adds new.
	void Initialize()
	{
		Data_Hiscore hiscores = DataManager.instance.GetHighscore();

		// if no records, initialize. Coupled. 
		if (hiscores.data.Count == 0)
		{
			GameObject line = Instantiate(prefab, content.transform);
			line.GetComponent<HiscoreListItem>().Initialize();
		}
		else
		{
			for (int i = 0; i < hiscores.data.Count; i++)
			{
				GameObject line = Instantiate(prefab, content.transform);
				line.GetComponent<HiscoreListItem>().Initialize(i + 1, hiscores.data [i]);
			}
		}
	}

	void DestroyAll()
	{
		foreach (var item in content.GetComponentsInChildren<HiscoreListItem>())
		{
			Destroy(item.gameObject);
		}
	}

}
