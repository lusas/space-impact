﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiscoreListItem : MonoBehaviour
{
	[SerializeField] Text text;

	// Could avoid overloading but handling of string join would happen outside..
	// both hiscore scripts are coupled.

	public void Initialize()
	{
		text.text = "No records yet!";
	}

	public void Initialize(int id, int score)
	{
		text.text = id.ToString() + ". " + score.ToString();
	}
}
